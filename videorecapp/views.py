from django.conf import settings
from django.shortcuts import render
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect, HttpResponse
from django.urls import reverse
from django.db import transaction
from django.db import IntegrityError
from django.db import connection
from django.core.mail import send_mail
from videorecapp import yt_actions
from videorecapp.forms import UserForm, UserProfileInfoForm, ContactForm
from videorecapp.models import Video, User
from videorecapp.colaborative_filtering import prepare_dataset, user_recommendations
from star_ratings.models import Rating


def index(request):
    total_users = User.objects.all().count()
    total_videos = Video.objects.all().count()
    total_ratings = Rating.objects.all().count()
    return render(request, 'videorecapp/index.html', {
        'total_users': total_users,
        'total_videos': total_videos,
        'total_ratings': total_ratings,
    })


def recommendations(request):
    get_user_ids_query = (
        "SELECT id "
        "FROM auth_user "
        "ORDER BY id ASC;"
    )
    get_anonymous_recommendations = ("SELECT title, embed_url "
                      "FROM videorecapp_video v "
                      "INNER JOIN star_ratings_rating r ON v.id=r.object_id "
                      "WHERE r.average=5"
                      )
    if request.user.is_authenticated:
        # create DB connection
        cursor = connection.cursor()
        # get all user ids
        cursor.execute(get_user_ids_query)
        result = list(cursor.fetchall())
        user_ids = [list(x)[0] for x in result]
        dataset = {}
        for user_id in user_ids:
            result = prepare_dataset(user_id)
            user_result = {}
            for key,value in result:
                user_result[key] = value
            dataset[user_id] = user_result
        recommended_for_user = user_recommendations(dataset, request.user.id)
        if recommended_for_user:
            get_user_recommendations = ("SELECT title, embed_url "
                                        "FROM videorecapp_video v "
                                        "WHERE id IN ({0})"
                                        ).format(','.join(str(video) for video in recommended_for_user))
            cursor.execute(get_user_recommendations)
            recommended = cursor.fetchall()
            return render(request, 'videorecapp/recommendations.html', {
                'range': range(15),
                'recommended': recommended
            })
        else:
            cursor.execute(get_anonymous_recommendations)
            recommended = cursor.fetchall()
            return render(request, 'videorecapp/recommendations.html', {'range': range(15), 'recommended': recommended})
    else:
        cursor = connection.cursor()
        cursor.execute(get_anonymous_recommendations)
        recommended = cursor.fetchall()
        return render(request, 'videorecapp/recommendations.html', {'range': range(15), 'recommended': recommended})


def contact(request):
    contact_form = ContactForm()
    return render(request, 'videorecapp/contact.html', {'contact_form': contact_form})


def register(request):

    registered = False

    if request.method == 'POST':

        user_form = UserForm(data=request.POST)
        profile_form = UserProfileInfoForm(data=request.POST)

        if user_form.is_valid() and profile_form.is_valid():

            user = user_form.save()
            user.set_password(user.password)
            user.save()

            profile = profile_form.save(commit=False)
            profile.user = user

            if 'profile_pic' in request.FILES:
                profile.profile_pic = request.FILES['profile_pic']

            profile.save()
            registered = True

        else:
            print(user_form.errors, profile_form.errors)

    else:
        user_form = UserForm()
        profile_form = UserProfileInfoForm()

    return render(request, 'videorecapp/register.html',
                  {
                      'user_form': user_form,
                      'profile_form': profile_form,
                      'registered': registered,
                  })


def user_login(request):

    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=username, password=password)

        if user:
            if user.is_active:
                login(request, user)
                return HttpResponseRedirect(reverse('videorecapp:index'))
            else:
                return HttpResponse('Account not active')
        else:
            print('*' * 20)
            print('Someone tried to login and failed!')
            print('Username: {0}\nPassword: {0}'.format(username, password))
            print('*' * 20)
            return HttpResponse('Invalid login credentials supplied!')
    else:
        return render(request, 'videorecapp/login.html', {})


@login_required
def user_logout(request):
    logout(request)
    return HttpResponseRedirect(reverse('videorecapp:index'))


def login_form(request):
    form = UserForm()
    return render(request, 'videorecapp/login.html', {'form': form})


@login_required
def video_list(request):
    if request.method == 'POST':
        search_query = request.POST.get('search_query')
        # second argument for youtube_search is the number of videos to retrieve
        videos = yt_actions.youtube_search(search_query, '15')
        fetched_videos = []
        fetched_video_ids = []
        # create a list of fetched videos as instances of Video model for further DB insert
        for video_id, video_data in videos.items():
            new_video_id = video_id
            fetched_video_ids.append(new_video_id)
            new_video_title = video_data['title']
            new_video_embed_url = video_data['embed_url']
            new_video = Video(yt_video_id=new_video_id, title=new_video_title, embed_url=new_video_embed_url)
            fetched_videos.append(new_video)
        # bulk save of all fetched_videos to DB
        bulk_save(fetched_videos)
        # get the video objects from DB
        videos_to_display = Video.objects.filter(yt_video_id__in=fetched_video_ids)
        return render(request, 'videorecapp/search.html', {'fetched_videos': videos_to_display})


def send_email(request):
    '''
    if request.method == 'POST':
        contact_form = ContactForm(data=request.POST)
        if contact_form.is_valid():
            subject = 'RecSys: Contact form feedback'
            from_email = settings.EMAIL_HOST_USER
            to_email = ['mellon211195@gmail.com']
            message = contact_form.cleaned_data['message']
            send_mail(subject=subject, from_email=from_email, recipient_list=to_email, message=message, fail_silently=False)
    '''
    return HttpResponseRedirect(reverse('videorecapp:index'))


def bulk_save(queryset):
    for item in queryset:
        try:
            with transaction.atomic():
                item.save()
        except IntegrityError:
            print('Duplicate entry found - {0}'.format(item.yt_video_id))
