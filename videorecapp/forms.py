from django import forms
from django.contrib.auth.models import User
from videorecapp.models import UserProfileInfo


class UserForm(forms.ModelForm):

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email', 'username', 'password')
        labels = {
            'first_name': 'First Name',
            'last_name': 'Last Name',
            'email': 'Email',
            'username': 'Username',
            'password': 'Password',
        }
        widgets = {
            'first_name': forms.TextInput(attrs={'class': 'comment_input'}),
            'last_name': forms.TextInput(attrs={'class': 'comment_input'}),
            'email': forms.EmailInput(attrs={'class': 'comment_input'}),
            'username': forms.TextInput(attrs={'class': 'comment_input'}),
            'password': forms.PasswordInput(attrs={'class': 'comment_input'}),
        }


class UserProfileInfoForm(forms.ModelForm):

    class Meta:
        model = UserProfileInfo
        fields = ('profile_pic',)
        labels = {
            'profile_pic': 'Profile picture'
        }


class ContactForm(forms.Form):
    name = forms.CharField(widget=forms.TextInput(attrs={'class': 'comment_input'}), label='Name')
    email = forms.EmailField(widget=forms.EmailInput(attrs={'class': 'comment_input'}), label='Return email')
    message = forms.CharField(widget=forms.Textarea(attrs={'class': 'comment_input'}), label='Message')


class LoginForm(forms.Form):
    username = forms.CharField()
    password = forms.PasswordInput()
