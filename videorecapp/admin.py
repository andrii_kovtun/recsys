from django.contrib import admin
from videorecapp.models import UserProfileInfo, Video
from star_ratings.models import UserRating, Rating

admin.site.register(UserProfileInfo)
admin.site.register(Video)

