from django.urls import path, re_path

from . import views

app_name = 'videorecapp'

urlpatterns = [
    path('', views.index, name='index'),
    path('recommendations/', views.recommendations, name='recommendations'),
    path('contact/', views.contact, name='contact'),
    path('register/', views.register, name='register'),
    path('user_login/', views.user_login, name='user_login'),
    path('send_email/', views.send_email, name='send_email'),
    path('video_list/', views.video_list, name='video_list'),
]
