# youtube interaction logic resides here

import os
from googleapiclient.discovery import build

DEVELOPER_KEY = os.environ.get('RECSYS_DEV_KEY')
YOUTUBE_API_SERVICE_NAME = 'youtube'
YOUTUBE_API_VERSION = 'v3'


def embed_video(youtube_video_id):
    embed_url = 'http://youtube.com/embed/{0}'.format(youtube_video_id)
    return embed_url


def youtube_search(query, max_videos):
    youtube = build(YOUTUBE_API_SERVICE_NAME, YOUTUBE_API_VERSION, developerKey=DEVELOPER_KEY)

    ## several parameters are to be specified to get the desired list of videos:
    # q               - the query string
    # part            - snippet
    # maxResults      - number of videos to return
    # type            - video (we don't need playlists or channels here)
    # order           - viewCount (get the most viewed videos)
    # safeSearch      - strict (YouTube will try to exclude all restricted content)
    # videoEmbeddable - true (only retrieve embeddable videos)
    # videoLicense    - creativeCommon (users can reuse videos with this license type)
    # videoCategoryId - id=27 (Education)
    search_response = youtube.search().list(
        q=query,
        part='id,snippet',
        maxResults=max_videos,
        type='video',
        order='viewCount',
        safeSearch='strict',
        videoEmbeddable='true',
        videoLicense='creativeCommon',
        videoCategoryId='27',
    ).execute()

    videos = {}
    # videos format:
    # videos = {
    #   'id': {
    #           'title': Video title,
    #           'embed_url': URL to use in <iframe> tag in html template
    #       }
    # }

    # add each result to the list of videos
    for search_result in search_response.get('items', []):
        videos[search_result['id']['videoId']] = {}
        videos[search_result['id']['videoId']]['title'] = search_result['snippet']['title']
        videos[search_result['id']['videoId']]['embed_url'] = embed_video(search_result['id']['videoId'])
    return videos
