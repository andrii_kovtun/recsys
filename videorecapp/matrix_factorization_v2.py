import pandas as pd
import numpy as np
from scipy.sparse.linalg import svds


def recommend_movies(userID, num_recommendations=5):

    ratings_list = [i.strip().split("::") for i in open('/tmp/recsys_ratings.txt', 'r').readlines()]
    users_list = [i.strip().split("::") for i in open('/tmp/recsys_users.txt', 'r').readlines()]
    movies_list = [i.strip().split("::") for i in open('/tmp/recsys_videos.txt', 'r').readlines()]

    ratings = np.array(ratings_list)
    users = np.array(users_list)
    movies = np.array(movies_list)

    ratings_df = pd.DataFrame(ratings_list, columns=['UserID', 'VideoID', 'Rating'], dtype=int)
    movies_df = pd.DataFrame(movies_list, columns=['VideoID', 'YTID', 'Title'])
    movies_df['VideoID'] = movies_df['VideoID'].apply(pd.to_numeric)

    R_df = ratings_df.pivot(index='UserID', columns='VideoID', values='Rating').fillna(0)

    R = R_df.values
    user_ratings_mean = np.mean(R, axis=1)
    R_demeaned = R - user_ratings_mean.reshape(-1, 1)

    U, sigma, Vt = svds(R_demeaned)
    sigma = np.diag(sigma)

    all_user_predicted_ratings = np.dot(np.dot(U, sigma), Vt) + user_ratings_mean.reshape(-1, 1)
    preds_df = pd.DataFrame(all_user_predicted_ratings, columns=R_df.columns)

    # Get and sort the user's predictions
    user_row_number = userID - 1  # UserID starts at 1, not 0
    sorted_user_predictions = preds_df.iloc[user_row_number].sort_values(ascending=False)  # UserID starts at 1

    # Get the user's data and merge in the movie information.
    user_data = ratings_df[ratings_df.UserID == (userID)]
    user_full = (user_data.merge(movies_df, how='left', left_on='VideoID', right_on='VideoID').
                 sort_values(['Rating'], ascending=False)
                 )

    # print('User {0} has already rated {1} movies.'.format(userID, user_full.shape[0]))
    # print('Recommending highest {0} predicted ratings movies not already rated.'.format(num_recommendations))

    # Recommend the highest predicted rating movies that the user hasn't seen yet.
    recommendations = (movies_df[~movies_df['VideoID'].isin(user_full['VideoID'])].
                           merge(pd.DataFrame(sorted_user_predictions).reset_index(), how='left',
                                 left_on='VideoID',
                                 right_on='VideoID').
                           rename(columns={user_row_number: 'Predictions'}).
                           sort_values('Predictions', ascending=False).
                           iloc[:num_recommendations, :-1]
                           )

    return user_full, recommendations


already_rated, predictions = recommend_movies(1, 10)
predictions_list = predictions['YTID'].tolist()
result_predictions = []
for vid in predictions_list:
    corrected = vid.strip('"')
    result_predictions.append(corrected)
