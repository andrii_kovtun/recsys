from django.db import models
from django.contrib.auth.models import User
from django.core.validators import MaxValueValidator, MinValueValidator


class UserProfileInfo(models.Model):

    user = models.OneToOneField(User, on_delete=models.CASCADE)

    # additional fields
    profile_pic = models.ImageField(upload_to='profile_pics', blank=True)

    def __str__(self):
        return self.user.username


class Video(models.Model):
    """
    defines a video record retrieved from YouTube:
    """
    yt_video_id = models.CharField(max_length=100, unique=True)
    title = models.CharField(max_length=105, unique=False, null=False)
    embed_url = models.URLField(max_length=100, null=True)
