import os
from django.db import connection


def prepare_datasets():
    filelist = ['/tmp/recsys_ratings.txt', '/tmp/recsys_users.txt', '/tmp/recsys_videos.txt']

    prepare_ratings_dataset = """
        SELECT sru.user_id,
           vv.id,
           sru.score 
        INTO outfile '/tmp/recsys_ratings.txt' 
        FIELDS TERMINATED BY '::' 
        OPTIONALLY ENCLOSED BY '"' 
        LINES TERMINATED BY '\n'
        FROM star_ratings_userrating sru
        JOIN star_ratings_rating srr ON sru.rating_id=srr.id
        JOIN auth_user au ON sru.user_id=au.id
        JOIN videorecapp_video vv ON srr.object_id=vv.id
        ORDER BY sru.user_id;
    """

    prepare_users_dataset = """
        SELECT id,
           username 
        INTO outfile '/tmp/recsys_users.txt' 
        FIELDS TERMINATED BY '::' 
        OPTIONALLY ENCLOSED BY '"' 
        LINES TERMINATED BY '\n'
        FROM auth_user
        ORDER BY id;
    """
    prepare_videos_dataset = """
        SELECT id,
           yt_video_id,
           title 
        INTO outfile '/tmp/recsys_videos.txt' 
        FIELDS TERMINATED BY '::' 
        OPTIONALLY ENCLOSED BY '"' 
        LINES TERMINATED BY '\n'
        FROM videorecapp_video
        ORDER BY id;
    """

    for dataset_file in filelist:
        if os.path.exists(dataset_file):
            os.chmod(dataset_file, 0o777)
            os.remove(dataset_file)
        cursor = connection.cursor()

        # prepare ratings dataset
        cursor.execute(prepare_ratings_dataset)
        result = list(cursor.fetchall())
        ratings_dataset = [list(x)[0] for x in result]

        # prepare users dataset
        cursor.execute(prepare_users_dataset)
        result = list(cursor.fetchall())
        users_dataset = [list(x)[0] for x in result]

        # prepare videos dataset
        cursor.execute(prepare_videos_dataset)
        result = list(cursor.fetchall())
        videos_dataset = [list(x)[0] for x in result]

    datasets = [ratings_dataset, users_dataset, videos_dataset]
    return datasets


prepare_datasets()
print('DOne')
